using namespace std;

void readImageFLT(float* img, const string& filename);

void readImagePPM(float* img, const string& filename);

void writeDistanceImageFLT(float* img, const string& filename, int imgSize, float maxDist);

void writeDistanceImagePPM(float* img, const string& filename, int imgSize, float maxDist);

void writeDistanceImageBin(float* img, const string& filename, int imgSize);

__device__ float edgedf(float2 g, float a);

__device__ float centroid_dist(int2 from, int2 to, float2 grad, float* img, int imgSize);

__device__ float dist(int2 from, int2 to, float2 grad, float* img, int imgSize);

__global__ void setup(int2 *closest, float2 *grad, float *img, int imgSize);
