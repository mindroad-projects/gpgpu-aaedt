#include <chrono>
#include <iostream>
#include "../include/common.h"
#include <stdio.h>

__global__ void propagateSites(int2 *closest, int2 *voronoi, float2 *grad, float *img, float *out, int imgSize, int stepSize) {

	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int idy = blockIdx.y * blockDim.y + threadIdx.y;
	int id = idy * imgSize + idx;
	int2 neighbors[9] = { {idx - stepSize, idy + stepSize},
							{idx, idy + stepSize},
							{idx + stepSize, idy + stepSize},
							{idx - stepSize, idy},
							{idx, idy},
							{idx + stepSize, idy},
							{idx - stepSize, idy - stepSize},
							{idx, idy - stepSize},
							{idx + stepSize, idy - stepSize}
	};
	float bestDist = imgSize * 3.0f;
	int2 bestSite = { -1, -1 };
	for (int i = 0; i < 9; ++i) {
		int2 n = neighbors[i];
		if (n.x >= imgSize || n.x < 0 || n.y >= imgSize || n.y < 0) continue;
		int2 nSite = closest[n.x + n.y * imgSize];

		float newDist = (nSite.x < 0) ? imgSize * 3.0f : dist({ idx, idy }, nSite, grad[nSite.x + nSite.y * imgSize], img, imgSize);
		//printf("New dist: %f\n", newDist);

		if (newDist < bestDist) {
			bestDist = newDist;
			bestSite = nSite;
		}

	}
	voronoi[id] = bestSite;
	out[id] = bestDist;
}

void JFA(float* edt, float* img, int size)
{
	float* dev_img = 0;
	float2* dev_grad = 0;
	float* dev_edt = 0;
	int2* dev_closest = 0;
	int2* dev_voronoi = 0;

	cudaError_t e;

	cudaSetDevice(0);

	cudaMalloc((void**)& dev_grad, size * size * sizeof(float2));
	cudaMalloc((void**)& dev_edt, size * size * sizeof(float));
	cudaMalloc((void**)& dev_img, size * size * sizeof(float));

	cudaMemcpy(dev_img, img, size * size * sizeof(float), cudaMemcpyHostToDevice);

	cudaMalloc((void**)& dev_closest, size * size * sizeof(int2));
	cudaMalloc((void**)& dev_voronoi, size * size * sizeof(int2));

	dim3 block = { 8, 8 };
	dim3 grid = { (unsigned int)size / 8, (unsigned int)size / 8 };

	setup <<<grid, block >>> (dev_closest, dev_grad, dev_img, size);
	cudaDeviceSynchronize();
  	auto start = std::chrono::high_resolution_clock::now();
	for (int i = size / 2; i > 0; i /= 2) {
		propagateSites <<<grid, block >>> (dev_closest, dev_voronoi, dev_grad, dev_img, dev_edt, size, i);
		e = cudaDeviceSynchronize();
		if (e != cudaSuccess) {
			printf("%s\n", cudaGetErrorName(e));
		}
		int2* tmp = dev_closest;
		dev_closest = dev_voronoi;
		dev_voronoi = tmp;
		
	}
	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
	std::cout << duration.count() << std::endl;

	cudaMemcpy(edt, dev_edt, size * size * sizeof(float), cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();

	cudaFree(dev_closest);
	cudaFree(dev_grad);
	cudaFree(dev_img);
	cudaFree(dev_edt);
	cudaFree(dev_voronoi);

	cudaDeviceReset();

	return;
}

int main(int argc, char **argv) {
  int imgSize = atoi(argv[3]);
  std::string inputFilename(argv[1]);
  std:: string outputFilename(argv[4]);
  float* image = new float[imgSize * imgSize];
  float* edt = new float[imgSize * imgSize];

  readImagePPM(image, inputFilename);

  JFA(edt, image, imgSize);
  float maxDist = 0.0f;
  for (int i = 0; i < imgSize * imgSize; ++i) {
    maxDist = max(maxDist, edt[i]);
  }
  writeDistanceImagePPM(edt, outputFilename, imgSize, maxDist);
  writeDistanceImageBin(edt, outputFilename, imgSize);

}
