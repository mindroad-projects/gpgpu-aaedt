#include <fstream>
#include <iostream>
#include <iomanip>
#include "../include/common.h"

using namespace std;

void readImageFLT(float* img, const string& filename) {
  ifstream file;
  file.open(filename);
  if (!file) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }
  int size;
  file >> size >> size;
  size = size * size;
  for (int i = 0; i < size; ++i) {
    file >> img[i];
  }
  file.close();

}

void readImagePPM(float* img, const string& filename) {
  ifstream file;
  string mode;
  int size;
  int colorDepth;
  file.open(filename);
  if (!file) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }
  file >> mode;
  file >> size >> size;
  file >> colorDepth;
  size = size * size;
  for (int i = 0; i < size; ++i) {
    int color;
    file >> color;
    if (mode == "P3") {
      file >> color >> color;
    }
    img[i] = (float)(color)/(float)(colorDepth);
  }
  file.close();

}

void writeDistanceImageFLT(float* img, const string& filename, int imgSize, float maxDist) {
  ofstream file;
  file.open(filename);
  if (!file) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }
  file << imgSize << " " << imgSize << endl;
  int id = 0;
  for (int y = 0; y < imgSize; ++y) {
    for (int x = 0; x < imgSize; ++x) {
      file << img[id++] << " "; 
    }
    file << endl;
  }
  file.close();
}

void writeDistanceImagePPM(float* img, const string& filename, int imgSize, float maxDist) {
  ofstream file;
  file.open(filename + ".pgm");
  if (!file) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }
  file << "P2" << endl << imgSize << " " << imgSize << endl << "255" << endl;
  int id = 0;
  for (int y = 0; y < imgSize; ++y) {
    for (int x = 0; x < imgSize; ++x) {
      int color = int((img[id++] / maxDist) * 255.0f);
      color = color <= 255 ? color : 255;
      file << color << " "; 
    }
    file << endl;
  }
  file.close();
}

void writeDistanceImageBin(float* img, const string& filename, int imgSize) {
	ofstream file;
	file.open(filename + ".bin", ios::binary);
	if (!file) {
		cerr << "Could not open " << filename << endl;
		exit(1);
	}
	int id = 0;
	for (int y = 0; y < imgSize; ++y) {
		for (int x = 0; x < imgSize; ++x) {
		file.write(reinterpret_cast<char *>(&img[id++]), sizeof(float));
		}
	}
	file.close();
}

__device__ float edgedf(float2 g, float a) {
	float df, glength, temp, a1;

	if ((g.x == 0) || (g.y == 0)) { // Either A) gx or gy are zero, or B) both
		df = 0.5 - a;  // Linear approximation is A) correct or B) a fair guess
	}
	else {
		glength = sqrtf(g.x * g.x + g.y * g.y);
		if (glength > 0) {
			g.x = g.x / glength;
			g.y = g.y / glength;
		}
		/* Everything is symmetric wrt sign and transposition,
		* so move to first octant (gx>=0, gy>=0, gx>=gy) to
		* avoid handling all possible edge directions.
		*/
		g.x = fabs(g.x);
		g.y = fabs(g.y);
		if (g.x < g.y) {
			temp = g.x;
			g.x = g.y;
			g.y = temp;
		}
		a1 = 0.5f * g.y / g.x;
		if (a < a1) { // 0 <= a < a1
			df = 0.5f * (g.x + g.y) - sqrt(2.0f * g.x * g.y * a);
		}
		else if (a < (1.0f - a1)) { // a1 <= a <= 1-a1
			df = (0.5f - a) * g.x;
		}
		else { // 1-a1 < a <= 1
			df = -0.5f * (g.x + g.y) + sqrt(2.0f * g.x * g.y * (1.0f - a));
		}
	}
	return df;
}


__device__ float centroid_dist(int2 from, int2 to, float2 grad, float* img, int imgSize) {

	if (to.x < 0 || to.y < 0) return imgSize * 3.0f;

	int id = to.y * imgSize + to.x;
	float a = img[id];
	if (a <= 0.0f) return imgSize * 3.0f;

	float dx = to.x - from.x;
	float dy = to.y - from.y;
	float di = sqrtf(dx * dx + dy * dy);
	return di;

}

__device__ float dist(int2 from, int2 to, float2 grad, float* img, int imgSize) {

	if (to.x < 0 || to.y < 0) return imgSize * 3.0f;

	int id = to.y * imgSize + to.x;
	float a = img[id];
	if (a > 1.0f) a = 1.0f;
	if (a < 0.0f) a = 0.0f;
	if (a == 0.0f) return imgSize * 3.0f;

	float dx = to.x - from.x;
	float dy = to.y - from.y;
	float di = sqrtf(dx * dx + dy * dy);

	float df;
	if (di == 0.0f) {
		df = fmaxf(edgedf(grad, a), 0.0f);
	}
	else {
		df = edgedf({ dx, dy }, a);
	}
	return di + df;
}

__global__ void setup(int2 *closest, float2 *grad, float *img, int imgSize){
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int idy = blockIdx.y * blockDim.y + threadIdx.y;
	int id = idy * imgSize + idx;
  #define SQRT2 1.4142136f
	if (img[id] > 0.0f) {
		closest[id].x = idx;
		closest[id].y = idy;

		if (idx > 0 && idx < imgSize - 1 && idy > 0 && idy < imgSize - 1) {
			grad[id].x = -img[id - imgSize - 1] - SQRT2 * img[id - 1] - img[id + imgSize - 1] + img[id - imgSize + 1] + SQRT2 * img[id + 1] + img[id + imgSize + 1];
			grad[id].y = -img[id - imgSize - 1] - SQRT2 * img[id - imgSize] - img[id - imgSize + 1] + img[id + imgSize - 1] + SQRT2 * img[id + imgSize] + img[id + imgSize + 1];

			float g = grad[id].x * grad[id].x + grad[id].y * grad[id].y;
			if (g > 0.0f) {
				g = sqrtf(g);
				grad[id].x /= g;
				grad[id].y /= g;
			}
		}
		else {
			grad[id].x = 0;
			grad[id].y = 0;

		}
	}
	else {
		closest[id].x = -1;
		closest[id].y = -1;

		grad[id].x = 0;
		grad[id].y = 0;
	}
}
