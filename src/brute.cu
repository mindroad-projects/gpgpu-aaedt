#include <chrono>
#include <iostream>
#include "../include/common.h"

__global__ void gpu_edt(int2 *closest, float2 *grad, float *img, float *out, int imgSize, int2 start) {

	int idx = blockIdx.x * blockDim.x + threadIdx.x + start.x;
	int idy = blockIdx.y * blockDim.y + threadIdx.y + start.y;
	int id = idx + imgSize * idy;

	float bestDist = imgSize * 3.0f;
	int2 bestSite = { -1, -1 };
	for (int y = 0; y < imgSize; ++y) {
	  for (int x = 0; x < imgSize; ++x) {
	    float newDist = dist({idx, idy}, {x, y}, grad[x + y * imgSize], img, imgSize);
	    if (newDist < bestDist){
	      bestDist = newDist;
	      bestSite = {x, y};
	    }
	  }
	}
	closest[id] = bestSite;
	out[id] = bestDist;
}

void BRUTE(float* edt, float* img, int size)
{
	float* dev_img = 0;
	float2* dev_grad = 0;
	float* dev_edt = 0;
	int2* dev_closest = 0;

	cudaSetDevice(0);

	cudaMalloc((void**)& dev_grad, size * size * sizeof(float2));
	cudaMalloc((void**)& dev_edt, size * size * sizeof(float));
	cudaMalloc((void**)& dev_img, size * size * sizeof(float));

	cudaMemcpy(dev_img, img, size * size * sizeof(float), cudaMemcpyHostToDevice);

	cudaMalloc((void**)& dev_closest, size * size * sizeof(int2));

	dim3 block = { 8, 8 };
	dim3 grid = { (unsigned int)size / 8, (unsigned int)size / 8 };

	setup <<<grid, block >>> (dev_closest, dev_grad, dev_img, size);
	cudaDeviceSynchronize();
	
	auto start = std::chrono::high_resolution_clock::now();
	
	int divs = 4;
	for (int y_div = 0; y_div < divs; ++y_div) {
		for (int x_div = 0; x_div < divs; ++x_div) {

			int2 start = {size * x_div/divs, size * y_div/divs};
			dim3 block = { 8, 8 };
			dim3 grid = { (unsigned int)size / (block.x * divs), (unsigned int)size / (block.y * divs) };

			cudaError_t e;
			gpu_edt <<<grid, block >>> (dev_closest, dev_grad, dev_img, dev_edt, size, start);
			e = cudaDeviceSynchronize();
			if (e != cudaSuccess) {
				printf("%s\n", cudaGetErrorName(e));
			}
		}
	}
	
	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
	std::cout << duration.count() << std::endl;
	cudaMemcpy(edt, dev_edt, size * size * sizeof(float), cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();

	cudaFree(dev_closest);
	cudaFree(dev_grad);
	cudaFree(dev_img);
	cudaFree(dev_edt);

	return;
}


int main(int argc, char **argv) {
  int imgSize = atoi(argv[3]);
  std::string inputFilename(argv[1]);
  std:: string outputFilename(argv[4]);
  float* image = new float[imgSize * imgSize];
  float* edt = new float[imgSize * imgSize];

  readImagePPM(image, inputFilename);

  BRUTE(edt, image, imgSize);
  float maxDist = 0.0f;
  for (int i = 0; i < imgSize * imgSize; ++i) {
    maxDist = max(maxDist, edt[i]);
  }
  writeDistanceImagePPM(edt, outputFilename, imgSize, maxDist);
  writeDistanceImageBin(edt, outputFilename, imgSize);

}
