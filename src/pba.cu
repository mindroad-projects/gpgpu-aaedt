#include <chrono>
#include <iostream>
#include "../include/common.h"

#define param1 8
#define param2 4
#define param3 512


__global__ void phase1Sweep(int2* closest, float2* grad, float* img, int m1, int imgSize)
{
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  int idy = threadIdx.y + blockDim.y * blockIdx.y;

  int bandId = idy * imgSize + idx * (imgSize / m1);
  int pointX = idx * (imgSize / m1) + 1;

  //Sweep right
  for (int i = bandId + 1; i < bandId + (imgSize / m1); ++i) {

    float oldDist = dist({ pointX, idy }, closest[i], grad[i], img, imgSize);
    float newDist = dist({ pointX, idy }, closest[i - 1], grad[i - 1], img, imgSize);


    if (newDist < oldDist) {
      closest[i] = closest[i - 1];
    }

    ++pointX;
  }

  pointX -= 2;
  //Sweep left
  for (int i = bandId + (imgSize / m1) - 2; i >= bandId; --i) {


    float oldDist = dist({ pointX, idy }, closest[i], grad[i], img, imgSize);
    float newDist = dist({ pointX, idy }, closest[i + 1], grad[i + 1], img, imgSize);

    /*if (idx == 0 && idy == 0){
      printf("%d: %f     %f     %f\n", pointX, oldDist, newDist, img[i]);
      }*/


    if (newDist < oldDist) {
      closest[i] = closest[i + 1];
    }
    --pointX;
  }

}


/*Needs to be launched in 1d blocks of size log(n) down to one, halved each step. (ySize number of blocks.) Then going down with doubling block size subtract 1. Both right and left*/
__global__ void phase1Prefix(int2* closest, float2* grad, float* img, int m1, int imgSize, int stepSize, bool up, bool right)
{
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  int idy = threadIdx.y + blockDim.y * blockIdx.y;

  int bid = 2 * (idx + 1) * stepSize - 1;
  int aid = bid - stepSize;

  if (!up) {
    bid += stepSize;
    aid += stepSize;
  }

  if (!right) {
    bid = 2 * m1 - bid - 1;
    aid = 2 * m1 - aid - 1;
  }

  int ax = (aid / 2 + aid % 2) * (imgSize / m1) - aid % 2;
  int bx = (bid / 2 + bid % 2) * (imgSize / m1) - bid % 2;

  aid = idy * imgSize + ax;
  bid = idy * imgSize + bx;
  /*if (idy == 0) printf("%d, %d\n",aid,bid);*/
  if (dist({ bx, idy }, closest[aid], grad[aid], img, imgSize) < dist({ bx, idy }, closest[bid], grad[bid], img, imgSize)) {
    closest[bid] = closest[aid];
  }

}


/*Updates the bands based on the edge-pixels closest sites*/
__global__ void phase1Update(int2* closest, float2* grad, float* img, int m1, int imgSize) {
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  int idy = threadIdx.y + blockDim.y * blockIdx.y;

  int bandId = idy * imgSize + idx * (imgSize / m1);
  int pointX = idx * (imgSize / m1) + 1;

  for (int i = bandId + 1; i < bandId + (imgSize / m1) - 1; ++i) {
    if (dist({ pointX, idy }, closest[bandId], grad[bandId], img, imgSize) < dist({ pointX, idy }, closest[i], grad[i], img, imgSize)) {
      closest[i] = closest[bandId];
    }
    if (dist({ pointX, idy }, closest[bandId + (imgSize / m1) - 1], grad[bandId + (imgSize / m1) - 1], img, imgSize) < dist({ pointX, idy }, closest[i], grad[i], img, imgSize)) {
      closest[i] = closest[bandId + (imgSize / m1) - 1];
    }

    ++pointX;
  }
}

//======================================================Phase2======================================================

__device__ float lineColumnIntersection(float2 a, float2 b, float c) {

  float a1 = b.y - a.y;
  float b1 = a.x - b.x;
  float c1 = a1 * a.x + b1 * a.y;

  float a2 = 1.0f;
  float b2 = 0.0f;
  float c2 = c;

  float det = a1 * b2 - a2 * b1;

  if (det == 0) return 99999999999.9f;

  return (a1 * c2 - a2 * c1) / det;
}

__device__ bool isDominating(float2 a, float2 b, float2 c, float column) {
  float2 p1 = { (a.x + b.x) / 2.0f, (a.y + b.y) / 2.0f };
  float2 p2 = { p1.x - (b.y - a.y), p1.y + (b.x - a.x) };

  float2 q1 = { (b.x + c.x) / 2.0f, (b.y + c.y) / 2.0f };
  float2 q2 = { q1.x - (c.y - b.y), q1.y + (c.x - b.x) };

  float p = lineColumnIntersection(p1, p2, column);
  float q = lineColumnIntersection(q1, q2, column);

  return p <= q;
}


/*Calculate the proximate sites for each band*/
__global__ void phase2sweep(int2 * closest, float2 * grad, float* img, int m2, int imgSize, int2* stk, int2* stkIndex)
{
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;

  int stkId = idx + idy * imgSize;
  int stkStart = idx + idy * imgSize * (imgSize / m2);
  int stkEnd = stkStart + imgSize * (imgSize / m2);

  //printf("%d, %d: Id: %d, Strt: %d, End: %d\n", idx, idy, stkId, stkStart / imgSize, stkEnd / imgSize);

  while (closest[stkStart].x == -1 && stkStart < stkEnd) {
    stkStart += imgSize;
  }

  if (stkStart >= stkEnd) {

    stkIndex[stkId].y = -1;
    stkIndex[stkId].x = -1;
    return;
  }

  stkIndex[stkId].x = stkStart;
  stkIndex[stkId].y = stkStart;


  for (int i = stkStart + imgSize; i < stkEnd; i += imgSize) {
    int top = stkIndex[stkId].y;
    int second = stk[top].x;

    if (closest[i].x == -1) {
      continue;
    }

    while (stkIndex[stkId].x != stkIndex[stkId].y) {

      int aid = closest[i].x + closest[i].y * imgSize;
      float2 ag = grad[aid];
      float adf = edgedf(ag, img[aid]);
      float agLength = sqrtf(ag.x * ag.x + ag.y * ag.y);
      if (agLength > 0.0f) {
	ag.x /= agLength;
	ag.y /= agLength;
      }
      int bid = closest[top].x + closest[top].y * imgSize;
      float2 bg = grad[bid];
      float bdf = edgedf(bg, img[bid]);
      float bgLength = sqrtf(bg.x * bg.x + bg.y * bg.y);
      if (bgLength > 0.0f) {
	bg.x /= bgLength;
	bg.y /= bgLength;
      }
      int cid = closest[second].x + closest[second].y * imgSize;
      float2 cg = grad[cid];
      float cdf = edgedf(cg, img[cid]);
      float cgLength = sqrtf(cg.x * cg.x + cg.y * cg.y);
      if (cgLength > 0.0f) {
	cg.x /= cgLength;
	cg.y /= cgLength;
      }

      float2 a = { closest[i].x + ag.x * adf, closest[i].y + ag.y * adf };
      float2 b = { closest[top].x + bg.x * bdf, closest[top].y + bg.y * bdf };
      float2 c = { closest[second].x + cg.x * cdf, closest[second].y + cg.y * cdf };
      if (isDominating(a, b, c, idx)) {
	stkIndex[stkId].y = second;
	top = second;
	second = stk[top].x;

      }
      else {
	break;
      }
    }
    stk[i].x = top;
    stk[top].y = i;
    stkIndex[stkId].y = i;

  }
  stk[stkIndex[stkId].y].y = -1;

}


//======================================================Phase3======================================================
/*Calculate 2d voronoi diagram*/
__global__ void phase3(int2* voronoi, int2* closest, float2* grad, float* out, int m3, float* img, int imgSize, int2* stk, int2* stkIndex, int baseY)
{


  __shared__ bool done;
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y + baseY;
  int id = idy * imgSize + idx;

  voronoi[id] = closest[stkIndex[idx].x];
  out[id] = dist({ idx, idy }, voronoi[id], grad[voronoi[id].x + voronoi[id].y * imgSize], img, imgSize);

  if (threadIdx.y == m3 - 1) done = false;
  __syncthreads();

  while (!done && stkIndex[idx].x != stkIndex[idx].y) {
    int a = stkIndex[idx].x;
    int b = stk[a].y;


    float distC = out[id];
    float distA = dist({ idx, idy }, closest[a], grad[closest[a].x + closest[a].y * imgSize], img, imgSize);
    float distB = dist({ idx, idy }, closest[b], grad[closest[b].x + closest[b].y * imgSize], img, imgSize);

    if (distA < distC && distA < distB) {
      voronoi[id] = closest[a];
      out[id] = distA;
      if (threadIdx.y == m3 - 1) done = true;
    }
    __syncthreads();
    if (distB < distA && distB < distC) {
      voronoi[id] = closest[b];
      out[id] = distB;
      stkIndex[idx].x = b;
    }
    else if (threadIdx.y == m3 - 1) {
      done = true;
    }
    __syncthreads();

  }

}


/*Calculate proximate sites for each column*/
__global__ void phase2merge(int2* closest, float2* grad, float* img, int m2, int imgSize, int2* stk, int2* stkIndex, int stepSize)
{
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  int stkLowId = idx + idy * imgSize * stepSize * 2;
  int stkHighId = stkLowId + stepSize * imgSize;
  int stkStart = stkIndex[stkLowId].x;
  int stkMid = stkIndex[stkHighId].x;
  int stkEnd = stkIndex[stkHighId].y;

  if (stkStart == -1) {
    stkIndex[stkLowId] = stkIndex[stkHighId];
    return;
  }
  if (stkEnd == -1) {
    return;
  }
  bool isAlmostMerged = false;
  for (int i = stkMid; i <= stkEnd && stk[i].y > 0; i = stk[i].y) {
    int top = stkIndex[stkLowId].y;
    int second = stk[top].x;
    while (stkIndex[stkLowId].x != stkIndex[stkLowId].y) {

      int aid = closest[i].x + closest[i].y * imgSize;
      float2 ag = grad[aid];
      float adf = edgedf(ag, img[aid]);
      float agLength = sqrtf(ag.x * ag.x + ag.y * ag.y);
      if (agLength > 0.0f) {
	ag.x /= agLength;
	ag.y /= agLength;
      }
      int bid = closest[top].x + closest[top].y * imgSize;
      float2 bg = grad[bid];
      float bdf = edgedf(bg, img[bid]);
      float bgLength = sqrtf(bg.x * bg.x + bg.y * bg.y);
      if (bgLength > 0.0f) {
	bg.x /= bgLength;
	bg.y /= bgLength;
      }
      int cid = closest[second].x + closest[second].y * imgSize;
      float2 cg = grad[cid];
      float cdf = edgedf(cg, img[cid]);
      float cgLength = sqrtf(cg.x * cg.x + cg.y * cg.y);
      if (cgLength > 0.0f) {
	cg.x /= cgLength;
	cg.y /= cgLength;
      }

      float2 a = { closest[i].x + ag.x * adf, closest[i].y + ag.y * adf };
      float2 b = { closest[top].x + bg.x * bdf, closest[top].y + bg.y * bdf };
      float2 c = { closest[second].x + cg.x * cdf, closest[second].y + cg.y * cdf };
      if (isDominating(a, b, c, idx)) {
	isAlmostMerged = false;
	stkIndex[stkLowId].y = second;
	top = second;
	second = stk[top].x;
      }
      else if (!isAlmostMerged) {
	isAlmostMerged = true;
	break;
      }
      else {
	stk[i].x = top;
	stk[top].y = i;
	stkIndex[stkLowId].y = stkIndex[stkHighId].y;
	return;
      }
    }
    stk[i].x = top;
    stk[top].y = i;
    stkIndex[stkHighId].x = stk[i].y;
    stkIndex[stkLowId].y = i;
  }
  stkIndex[stkLowId].y = stkIndex[stkHighId].y;


}





void PBA(float* edt, float* img, int size)
{
  float* dev_img = 0;
  float2* dev_grad = 0;
  int2* dev_closest = 0;
  float* dev_edt = 0;
  int2* dev_stack = 0;
  int2* dev_stackIndex = 0;
  int2* dev_voronoi = 0;

  cudaSetDevice(0);
  cudaMalloc((void**)& dev_closest, size * size * sizeof(int2));
  cudaMalloc((void**)& dev_grad, size * size * sizeof(float2));
  cudaMalloc((void**)& dev_edt, size * size * sizeof(float));
  cudaMalloc((void**)& dev_img, size * size * sizeof(float));
  cudaMalloc((void**)& dev_stack, size * size * sizeof(int2));
  cudaMalloc((void**)& dev_stackIndex, size * param2 * sizeof(int2));
  cudaMalloc((void**)& dev_voronoi, size * size * sizeof(int2));

  cudaMemcpy(dev_img, img, size * size * sizeof(float), cudaMemcpyHostToDevice);

  cudaMalloc((void**)& dev_voronoi, size * size * sizeof(int2));

  dim3 block = { 8,8 };
  dim3 grid = { (unsigned int)size / 8, (unsigned int)size / 8 };
  auto start = std::chrono::high_resolution_clock::now();
  setup << <grid, block >> > (dev_closest, dev_grad, dev_img, size);
  cudaDeviceSynchronize();


  //======================================================Phase1======================================================

  block = { param1, 32 };
  grid = { 1 , (unsigned int)size / 32 };

  phase1Sweep << <grid, block >> > (dev_closest, dev_grad, dev_img, param1, size);
  cudaDeviceSynchronize();

  for (unsigned int i = 1; i <= param1; i *= 2) {
    block = { param1 / i, 32 };
    grid = { 1, (unsigned int)size / 32 };
    phase1Prefix << <grid, block >> > (dev_closest, dev_grad, dev_img, param1, size, i, true, true);
    cudaDeviceSynchronize();
  }
  for (unsigned int i = param1 / 2; i >= 1; i /= 2) {
    block = { param1 / i - 1, 32, };
    grid = { 1, (unsigned int)size / 32 };
    phase1Prefix << <grid, block >> > (dev_closest, dev_grad, dev_img, param1, size, i, false, true);
    cudaDeviceSynchronize();
  }

  for (unsigned int i = 1; i <= param1; i *= 2) {
    block = { param1 / i, 32 };
    grid = { 1, (unsigned int)size / 32 };
    phase1Prefix << <grid, block >> > (dev_closest, dev_grad, dev_img, param1, size, i, true, false);
    cudaDeviceSynchronize();
  }
  for (unsigned int i = param1 / 2; i >= 1; i /= 2) {
    block = { param1 / i - 1, 32 };
    grid = { 1, (unsigned int)size / 32 };
    phase1Prefix << <grid, block >> > (dev_closest, dev_grad, dev_img, param1, size, i, false, false);
    cudaDeviceSynchronize();
  }

  block = { param1, 32 };
  grid = { 1, (unsigned int)size / 32 };
  phase1Update << <grid, block >> > (dev_closest, dev_grad, dev_img, param1, size);
  cudaDeviceSynchronize();


  //======================================================Phase2======================================================



  block = { 32, param2 };
  grid = { (unsigned int)size / 32, 1 };

  phase2sweep << <grid, block >> > (dev_closest, dev_grad, dev_img, param2, size, dev_stack, dev_stackIndex);
  cudaDeviceSynchronize();



  for (int i = 1; i < param2; i <<= 1) {
    block = { 32, param2 / (unsigned int)(i * 2) };
    grid = { (unsigned int)size / 32, 1 };

    phase2merge << <grid, block >> > (dev_closest, dev_grad, dev_img, param2, size, dev_stack, dev_stackIndex, i);
    cudaDeviceSynchronize();
  }


  //======================================================Phase3======================================================
  block = { 1, param3 };
  grid = { (unsigned int)size, 1 };
  for (int i = 0; i < size / param3; ++i) {

    phase3 << <grid, block >> > (dev_voronoi, dev_closest, dev_grad, dev_edt, param3, dev_img, size, dev_stack, dev_stackIndex, i * param3);
    cudaDeviceSynchronize();
  }

  //======================================================Done========================================================


  cudaDeviceSynchronize();

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  std::cout << duration.count() << std::endl;

  cudaMemcpy(edt, dev_edt, size * size * sizeof(float), cudaMemcpyDeviceToHost);
  cudaDeviceSynchronize();

  cudaFree(dev_closest);
  cudaFree(dev_grad);
  cudaFree(dev_img);
  cudaFree(dev_edt);
  cudaFree(dev_stack);
  cudaFree(dev_stackIndex);
  cudaFree(dev_voronoi);

  return;
}

int main(int argc, char **argv) {
  int imgSize = atoi(argv[3]);
  std::string inputFilename(argv[1]);
  std:: string outputFilename(argv[4]);
  float* image = new float[imgSize * imgSize];
  float* edt = new float[imgSize * imgSize];

  readImagePPM(image, inputFilename);

  PBA(edt, image, imgSize);
  float maxDist = 0.0f;
  for (int i = 0; i < imgSize * imgSize; ++i) {
    maxDist = max(maxDist, edt[i]);
  }
  writeDistanceImagePPM(edt, outputFilename, imgSize, maxDist);
  writeDistanceImageBin(edt, outputFilename, imgSize);

}
