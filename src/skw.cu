#include <chrono>
#include <iostream>
#include "../include/common.h"

__device__ void updatePixel(int2* voronoi, float2* grad, float* img, float* out, int imgSize, int2* neighbors, int idx, int idy, int id) {


	float bestDist = imgSize * 3.0f;
	int2 bestSite = { -1, -1 };
	for (int i = 0; i < 4; ++i) {
		int2 n = neighbors[i];
		if (n.x >= imgSize || n.x < 0 || n.y >= imgSize || n.y < 0) continue;
		int2 nSite = voronoi[n.x + n.y * imgSize];

		float newDist = (nSite.x < 0) ? imgSize * 3.0f : dist({ idx, idy }, nSite, grad[nSite.x + nSite.y * imgSize], img, imgSize);

		if (newDist < bestDist) {
			bestDist = newDist;
			bestSite = nSite;
		}

	}
	voronoi[id] = bestSite;
	out[id] = bestDist;
}

__global__ void sweepRight(int2* voronoi, float2* grad, float* img, float* out, int imgSize, int iteration) {

	int idx = blockIdx.x * blockDim.x + threadIdx.x + iteration;
	int idy = blockIdx.y * blockDim.y + threadIdx.y;
	int id = idy * imgSize + idx;
	int2 neighbors[4];
	neighbors[0] = { idx, idy };
	neighbors[1] = { idx - 1, idy + 1 };
	neighbors[2] = { idx - 1, idy };
	neighbors[3] = { idx - 1, idy - 1 };

	updatePixel(voronoi, grad, img, out, imgSize, neighbors, idx, idy, id);
}

__global__ void sweepLeft(int2* voronoi, float2* grad, float* img, float* out, int imgSize, int iteration) {

	int idx = blockIdx.x * blockDim.x + threadIdx.x + iteration;
	int idy = blockIdx.y * blockDim.y + threadIdx.y;
	int id = idy * imgSize + idx;
	int2 neighbors[4];
	neighbors[0] = { idx, idy };
	neighbors[1] = { idx + 1, idy + 1 };
	neighbors[2] = { idx + 1, idy };
	neighbors[3] = { idx + 1, idy - 1 };

	updatePixel(voronoi, grad, img, out, imgSize, neighbors, idx, idy, id);
}

__global__ void sweepDown(int2* voronoi, float2* grad, float* img, float* out, int imgSize, int iteration) {

	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int idy = blockIdx.y * blockDim.y + threadIdx.y + iteration;
	int id = idy * imgSize + idx;
	int2 neighbors[4];
	neighbors[0] = { idx, idy };
	neighbors[1] = { idx - 1, idy - 1 };
	neighbors[2] = { idx, idy - 1};
	neighbors[3] = { idx + 1, idy - 1 };

	updatePixel(voronoi, grad, img, out, imgSize, neighbors, idx, idy, id);
}

__global__ void sweepUp(int2* voronoi, float2* grad, float* img, float* out, int imgSize, int iteration) {

	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int idy = blockIdx.y * blockDim.y + threadIdx.y + iteration;
	int id = idy * imgSize + idx;
	int2 neighbors[4];
	neighbors[0] = { idx, idy };
	neighbors[1] = { idx - 1, idy + 1 };
	neighbors[2] = { idx, idy + 1};
	neighbors[3] = { idx + 1, idy + 1 };

	updatePixel(voronoi, grad, img, out, imgSize, neighbors, idx, idy, id);
}

void SKW(float* edt, float* img, int size)
{
	float* dev_img = 0;
	float2* dev_grad = 0;
	float* dev_edt = 0;
	int2* dev_voronoi = 0;

	cudaSetDevice(0);
	cudaMalloc((void**)& dev_grad, size * size * sizeof(float2));
	cudaMalloc((void**)& dev_edt, size * size * sizeof(float));
	cudaMalloc((void**)& dev_img, size * size * sizeof(float));

	cudaMemcpy(dev_img, img, size * size * sizeof(float), cudaMemcpyHostToDevice);

	cudaMalloc((void**)& dev_voronoi, size * size * sizeof(int2));

	dim3 block = { 8, 8 };
	dim3 grid = { (unsigned int)size / 8, (unsigned int)size / 8 };

	setup << <grid, block >> > (dev_voronoi, dev_grad, dev_img, size);
	cudaDeviceSynchronize();
	auto start = std::chrono::high_resolution_clock::now();
	block = { 1, 32 };
	grid = { 1, (unsigned int)size / 32 };

	for (int i = 1; i < size; ++i) {
		sweepRight<<<grid, block >>> (dev_voronoi, dev_grad, dev_img, dev_edt, size, i);
		cudaDeviceSynchronize();
	}
	for (int i = size - 2; i >= 0; --i) {
		sweepLeft<<<grid, block >>> (dev_voronoi, dev_grad, dev_img, dev_edt, size, i);
		cudaDeviceSynchronize();
	}

	block = { 32, 1 };
	grid = { (unsigned int)size / 32, 1 };
	for (int i = 1; i < size; ++i) {
		sweepDown<< <grid, block >> > (dev_voronoi, dev_grad, dev_img, dev_edt, size, i);
		cudaDeviceSynchronize();
	}
	for (int i = size - 2; i >= 0; --i) {
		sweepUp<< <grid, block >> > (dev_voronoi, dev_grad, dev_img, dev_edt, size, i);
		cudaDeviceSynchronize();
	}

	auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  std::cout << duration.count() << std::endl;

	cudaMemcpy(edt, dev_edt, size * size * sizeof(float), cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();

	cudaFree(dev_grad);
	cudaFree(dev_img);
	cudaFree(dev_edt);
	cudaFree(dev_voronoi);

	return;
}

int main(int argc, char **argv) {
  int imgSize = atoi(argv[3]);
  std::string inputFilename(argv[1]);
  std:: string outputFilename(argv[4]);
  float* image = new float[imgSize * imgSize];
  float* edt = new float[imgSize * imgSize];

  readImagePPM(image, inputFilename);

  SKW(edt, image, imgSize);
  float maxDist = 0.0f;
  for (int i = 0; i < imgSize * imgSize; ++i) {
    maxDist = max(maxDist, edt[i]);
  }
  writeDistanceImagePPM(edt, outputFilename, imgSize, maxDist);
  writeDistanceImageBin(edt, outputFilename, imgSize);

}
