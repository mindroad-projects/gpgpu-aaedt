IMAGE_NAMES := 13_10 13_4 13_8 17_12 17_16 17_2 17_6 13_1 13_5 13_9 17_13 17_17 17_3 17_7 13_2 13_6 17_10 17_14 17_18 17_4 17_8 13_3 13_7 17_11 17_15 17_1 17_5 17_9
ARCH := sm_30

.PHONY : images
images:
	for IMG in ${IMAGE_NAMES};\
	do\
	    	mogrify -format pgm -compress none -background black -flatten -channel RGB -negate -path res/ tattoo/$${IMG}.png;\
		mv res/$${IMG}.pgm res/neg_$${IMG}.pgm;\
		mogrify -format pgm -compress none -background white -flatten -path res/ tattoo/$${IMG}.png;\
	done

all: skw jfa brute pba pba-edt

skw : build/skw.o build/common.o
	nvcc -arch=${ARCH} -o skw build/skw.o build/common.o

jfa : build/jfa.o build/common.o
	nvcc -arch=${ARCH} -o jfa build/jfa.o build/common.o

brute : build/brute.o build/common.o
	nvcc -arch=${ARCH} -o brute build/brute.o build/common.o

pba : build/pba.o build/common.o
	nvcc -arch=${ARCH} -o pba build/pba.o build/common.o

pba-edt : build/pba-edt.o build/common.o
	nvcc -arch=${ARCH} -o pba-edt build/pba-edt.o build/common.o

build/skw.o : src/skw.cu
	nvcc -arch=${ARCH} -dc src/skw.cu -o build/skw.o

build/jfa.o : src/jfa.cu
	nvcc -arch=${ARCH} -dc src/jfa.cu -o build/jfa.o

build/brute.o : src/brute.cu
	nvcc -arch=${ARCH} -dc src/brute.cu -o build/brute.o

build/pba.o : src/pba.cu
	nvcc -arch=${ARCH} -dc src/pba.cu -o build/pba.o

build/pba-edt.o : src/pba-edt.cu
	nvcc -arch=${ARCH} -dc src/pba-edt.cu -o build/pba-edt.o

build/common.o : src/common.cu
	nvcc -arch=${ARCH} -dc src/common.cu -o build/common.o

.PHONY : clean
clean :
	rm -f build/skw.o build/common.o build/jfa.o build/brute.o build/pba.o skw jfa brute pba
	rm res/*.pgm
	rm out/*.pgm

.PHONY : run
run :
	for IMG in ${IMAGE_NAMES};\
	do\
		./brute res/$${IMG}.pgm 0 512 out/brute_$${IMG};\
		./brute res/neg_$${IMG}.pgm 0 512 out/brute_neg_$${IMG};\
		./skw res/$${IMG}.pgm 0 512 out/skw_$${IMG};\
		./skw res/neg_$${IMG}.pgm 0 512 out/skw_neg_$${IMG};\
		./jfa res/$${IMG}.pgm 0 512 out/jfa_$${IMG};\
		./jfa res/neg_$${IMG}.pgm 0 512 out/jfa_neg_$${IMG};\
		./pba res/$${IMG}.pgm 0 512 out/pba_$${IMG};\
		./pba res/neg_$${IMG}.pgm 0 512 out/pba_neg_$${IMG};\
		./pba-edt res/$${IMG}.pgm 0 512 out/pba-edt_$${IMG};\
		./pba-edt res/neg_$${IMG}.pgm 0 512 out/pba-edt_neg_$${IMG};\
	done


.PHONY : compare
compare :
	python3 ./scripts/compare.py ${IMAGE_NAMES}