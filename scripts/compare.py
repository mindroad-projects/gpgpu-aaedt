#!bin/python3
import struct
import sys
import itertools

#oracle, image = sys.argv[1:3]

def count_errors(oracle, image):

    with open(oracle, "rb") as ref_img, open(image, "rb") as img:
        ref_array = map(lambda a : a[0], struct.iter_unpack("f",ref_img.read()))
        array = map(lambda a : a[0], struct.iter_unpack("f",img.read()))
        resulting_diffs = []
        for correct_dist, calculated_dist in zip(ref_array, array):
            #if calculated_dist - correct_dist != 0.0:
            resulting_diffs.append(calculated_dist - correct_dist)
        resulting_diffs.sort()
        histogram = [0]
        i = 0
        print(resulting_diffs[-1])
        
        for diff in resulting_diffs:
            while (diff * 100) > i:
                i += 1
                histogram.append(0)
            histogram[i] += 1
        return histogram
        

def print_histogram(histogram):
    for i, c in enumerate(histogram):
        print(f"({i - 1}, {c}) ", end='')
    print()


def main():
    images = sys.argv[1:]
    algorithms = ["jfa", "skw", "pba", "pba-edt"]
    for algorithm in algorithms:
        e_count = []
        for img in images:
            e_count.append(count_errors(f"out/brute_{img}.bin", f"out/{algorithm}_{img}.bin"))
        final_histogram = [sum(x) for x in itertools.zip_longest(*e_count, fillvalue=0)]
        print(algorithm)
        print_histogram(final_histogram)
        


if __name__ == '__main__':
    main()